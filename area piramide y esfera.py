def area_esf(Radio):
    """
*********************************************************************************
		    Instituto Tecnol�gico de Costa Rica
		    Ingeier�a en Computadores
		    Programa: area_esf
		    Lenguaje: Python 3.6.4
		    Autor: Sahid Rojas Chac�n y Jos� Solano Mora
		    Versi�n: 1.0
		    Fecha de �ltima modificaci�n: Abril 26/2018

		    Entradas: De una esfera: Radio
		    Restricciones: -
		    Salidas: �rea de la esfera

*********************************************************************************
"""
    from math import pi
    �rea=4*pi*Radio**2
    return �rea
#Sahid el negro

def area_pir(Base,Altura):
    """
*********************************************************************************
		    Instituto Tecnol�gico de Costa Rica
		    Ingeier�a en Computadores
		    Programa: area_pir
		    Lenguaje: Python 3.6.4
		    Autor: Sahid Rojas Chac�n y Jos� Solano Mora
		    Versi�n: 1.0
		    Fecha de �ltima modificaci�n: Abril 26/2018

		    Entradas: De una piramide con base cuadrada: Base y altura
		    Restricciones: -
		    Salidas: �rea de la pir�mide con base cuadrada

*********************************************************************************
"""
    �reaL=3*(Base*Altura/2)
    �reaB=Base*Base
    �reaP=�reaL+�reaB
    return �reaP
